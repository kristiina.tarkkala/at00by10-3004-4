const expect = require('chai').expect;
const converter = require('../src/converter');

describe('src/converter.js', () => {

    describe('RGB to Hex', () => {
        
        it('should convert basic colors', () => {                      
            // arrange / act
            const redHex = converter.rgbToHex(255, 0, 0);
            const greenHex = converter.rgbToHex(0, 255, 0);
            const blueHex = converter.rgbToHex(0, 0, 255);

            // assert
            expect(redHex).to.equal('ff0000');
            expect(greenHex).to.equal('00ff00');
            expect(blueHex).to.equal('0000ff');
        });

    });


    describe('Hex to RGB', () => {

        it('should convert basic colors', () => {                      
            // arrange / act
            const redRgb = converter.hexToRgb('ff0000');
            const greenRgb = converter.hexToRgb('00ff00');
            const blueRgb = converter.hexToRgb('0000ff');

            // assert
            expect(redRgb).to.equal('255, 0, 0');
            expect(greenRgb).to.equal('0, 255, 0');
            expect(blueRgb).to.equal('0, 0, 255');
        });

    });


});