const expect = require('chai').expect;
const request = require('request');
const app = require('../src/server');
const port = 5001;


describe('src/server.js integration tests', () => {
    let server = undefined;
    before('start server', (done) => {
        server = app.listen(port, () => {
            console.log('server listening on localhost:'+port);
            done();
        });
    });
    after('close server', (done) => {
        server.close();
        console.log('server closed\n');
        done();
    });

    describe('RGB to Hex conversion', () => {
        function url(r,g,b) {
            return `http://localhost:${port}/rgb-to-hex?red=${r}&green=${g}&blue=${b}`;
        }

        it('should return status 200 OK', (done) => {
            let uri = url(0,0,0);
            request(uri, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it('should return correct Hex value', (done) => {
            let uri = url(255,255,255);
            request(uri, (error, response, body) => {
                expect(error).to.equal(null);
                expect(body).to.equal('ffffff');
                done();
            });
        });
    });

    describe('Hex to RGB conversion', () => {
        function url(hex) {
            return `http://localhost:${port}/hex-to-rgb?hex=${hex}`;
        }

        it('should return status 200 OK', (done) => {
            let uri = url('ffffff');
            request(uri, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it('should return correct RGB value', (done) => {
            let uri = url("ff00ff");
            request(uri, (error, response, body) => {
                console.log(body);
                expect(body).to.equal('255, 0, 255');
                done();
            });
        });

        it('should return correct RGB value with modifier 0x', (done) => {
            let uri = url("0xff00ff");
            request(uri, (error, response, body) => {
                expect(body).to.equal('255, 0, 255');
                done();
            });
        });

        it('should throw for too long hex', (done) => {
            let uri = url("0000000"); // 7 digits
            
            // Act & Assert
            expect(() => request(uri, (error, response, body))).to.throw(); // TODO: this hands out false positive when we don't test error message - currently there's something wrong with test infra and we get 'error is not defined'
            done();
        });
    });

});
