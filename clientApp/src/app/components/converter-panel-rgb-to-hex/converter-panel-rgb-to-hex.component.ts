import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { filter, first, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-converter-panel-rgb-to-hex',
  templateUrl: './converter-panel-rgb-to-hex.component.html',
  styleUrls: ['./converter-panel-rgb-to-hex.component.css']
})
export class ConverterPanelRgbToHexComponent implements OnInit {
  public red: number = 0;
  public green: number = 0;
  public blue: number = 0;
  public result: string = ""; 

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  public setValue() {
    this.getHex(this.red, this.green, this.blue).subscribe((res) => {
      this.result = res;
    });
  }

  private baseUrl = 'http://localhost:5001/'
  getHex(r: number, g: number, b: number) {
    const url = `${this.baseUrl}rgb-to-hex?red=${r}&green=${g}&blue=${b}`;    

    return this.http.get(url, { responseType: 'text'}).pipe(
      first(),
      // filter(result => !!this.red && !!this.blue && !!this.green),
      map(result => result),
      tap(result => console.log(result))
      // TODO error handling
    );  
  }

}
