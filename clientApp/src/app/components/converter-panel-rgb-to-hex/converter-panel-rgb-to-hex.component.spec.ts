import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterPanelRgbToHexComponent } from './converter-panel-rgb-to-hex.component';

describe('ConverterPanelRgbToHexComponent', () => {
  let component: ConverterPanelRgbToHexComponent;
  let fixture: ComponentFixture<ConverterPanelRgbToHexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterPanelRgbToHexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConverterPanelRgbToHexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
