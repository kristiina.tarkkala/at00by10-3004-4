import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterPanelHexToRgbComponent } from './converter-panel-hex-to-rgb.component';

describe('ConverterPanelHexToRgbComponent', () => {
  let component: ConverterPanelHexToRgbComponent;
  let fixture: ComponentFixture<ConverterPanelHexToRgbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterPanelHexToRgbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConverterPanelHexToRgbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
