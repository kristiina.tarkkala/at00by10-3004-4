import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatOptionSelectionChange } from '@angular/material/core';
import { catchError, filter, first, map, Observable, tap } from 'rxjs';

@Component({
  selector: 'app-converter-panel-hex-to-rgb',
  templateUrl: './converter-panel-hex-to-rgb.component.html',
  styleUrls: ['./converter-panel-hex-to-rgb.component.css']
})
export class ConverterPanelHexToRgbComponent implements OnInit {
  public value: string = "";
  public result: any = ""; 

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  public setValue() {
    this.getRgb().subscribe((res) => {
      this.result = res;
    });
  }

  private baseUrl = 'http://localhost:5001/'
  public getRgb() {
    const url = `${this.baseUrl}hex-to-rgb?hex=${this.value}`;    
    return this.http.get(url, { responseType: 'text'}).pipe(
      first(),
      filter(result => !!this.value),
      map(result => result),
      tap(result => console.log(result))
      // TODO error handling
    );
  }


}
