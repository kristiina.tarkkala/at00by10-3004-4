import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ConverterPanelHexToRgbComponent } from './components/converter-panel-hex-to-rgb/converter-panel-hex-to-rgb.component';

import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConverterPanelRgbToHexComponent } from './components/converter-panel-rgb-to-hex/converter-panel-rgb-to-hex.component';
import { MatFormFieldControl, MatFormFieldModule, MatLabel } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AsyncPipe } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    ConverterPanelHexToRgbComponent,
    ConverterPanelHexToRgbComponent,
    ConverterPanelRgbToHexComponent,
    // AsyncPipe,
    // MatLabel
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
