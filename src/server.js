const express = require('express');
const converter = require('./converter');
const app = express();
const port = 5001;

app.get('/', (req, res) => res.send('200'));

// endpoint http://localhost:5001/rgb-to-hex?red=255&green=0&blue=0
app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.red, 10);
    const green = parseInt(req.query.green, 10);
    const blue = parseInt(req.query.blue, 10);

    console.log(`got: ${red},${green},${blue}`);

    const result = converter.rgbToHex(red, green, blue);
    console.log(`send: ${result}`);

    res.setHeader('Access-Control-Allow-Origin', '*'); // Allow requests from all origins
    res.send(result);
});

// endpoint http://localhost:5001/hex-to-rgb?hex=ff0000
app.get('/hex-to-rgb', (req, res) => {
    let hex = req.query.hex.replace('0x', ''); // remove typical hexadecimal indicators

    console.log(`got: ${hex}`);

    if (hex.length != 6)
    {
        throw 'please provide proper hexadecimal value with six digits';
    }

    const result = converter.hexToRgb(hex);

    console.log(`send: ${result}`);

    res.setHeader('Access-Control-Allow-Origin', '*'); // Allow requests from all origins
    res.send(result);
});

if (process.env.NODE_ENV === 'test') {
        module.exports = app;
} else {
    app.listen(port, () => console.log('server listening on localhost:'+port));
}
