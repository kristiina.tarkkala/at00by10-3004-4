const pad = (hex) => {
    return (hex.length === 1 ? '0' + hex : hex);
}

module.exports = {   
/**
 * Convert RGB value to Hex
 * @param {Number} red 
 * @param {Number} green 
 * @param {Number} blue 
 * @returns {String}
 */
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        return pad(redHex) + pad(greenHex) + pad(blueHex);
    },

/**
 * Convert Hex value to RGB
 * @param {String} hex
 * @returns {String}
 */
    hexToRgb: (hex) => {
        const redHex = hex.substring(0,2);
        const greenHex = hex.substring(2,4);
        const blueHex = hex.substring(4,6);

        const redRgb = parseInt(redHex, 16);
        const greenRgb = parseInt(greenHex, 16);
        const blueRgb = parseInt(blueHex, 16);

        return `${redRgb}, ${greenRgb}, ${blueRgb}`;
    }
}